.PHONY: test_packer_aws_project terraform_aws_module
SHELL := /bin/bash

test_packer_aws_project:
	python3 -m venv cookiecutter_test_venv && \
	source ./cookiecutter_test_venv/bin/activate  && \
	pip install cookiecutter==2.1.1 && \
	cookiecutter templates/packer_aws_project \
		--config-file test/packer_aws_project/config.yaml \
		--no-input \
		--overwrite-if-exists

terraform_aws_module:
	python3 -m venv cookiecutter_test_venv && \
	source ./cookiecutter_test_venv/bin/activate  && \
	pip install cookiecutter==2.1.1 && \
	cookiecutter templates/terraform_aws_module \
		--config-file test/terraform_aws_module/config.yaml \
		--no-input \
		--overwrite-if-exists