# CC Team Templates

This repository contains the templates to start a new terraform or package module/project
using [cookiecutter](https://cookiecutter.readthedocs.io/en/stable/README.html) a command-line 
utility that creates projects from cookiecutters (project templates). 

# Prerequisites

* Python 3.7, 3.8, 3.9, 3.10
* Cookiecuter: [Available via pip or brew installation](https://cookiecutter.readthedocs.io/en/stable/installation.html)

# Templates availables

## packer_aws_project
AWS packer project.
## terraform_aws_module
AWS terraform module There are two make targets available test and destroy the test target runs the init, plan and apply
terraform commands, the destroy target runs the destroy terraform command. After the creation of the folder change the
variable AWS_PROFILE in the docker_compose file and run the commands:

```
make test
make destroy
```


# Getting Started

You don't have to clone this repo. Install cookiecutter CLI and run this command:

```
cookiecutter https://gitlab.ebi.ac.uk/davidg/ebi-cloud-templates --directory templates/terraform_aws_module
```

You can change the directory variable value depending on the template you need.

```
cookiecutter https://gitlab.ebi.ac.uk/davidg/ebi-cloud-templates --directory templates/packer_aws_project
```

This command will prompt the parameters needed for this template and will create a folder with the template structure

