terraform {
  required_version = ">=1.4.2"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "{{ cookiecutter.aws_provider_version }}"
    }
    random = {
      source = "hashicorp/random"
      version = "{{ cookiecutter.random_provider_version }}"
    }
  }
}
