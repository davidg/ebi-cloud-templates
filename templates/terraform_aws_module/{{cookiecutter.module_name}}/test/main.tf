provider "aws" {
  region  = "us-west-2"
}

module "{{ cookiecutter.module_name }}" {
  source = "/module/{{ cookiecutter.module_name }}"
  instance_type = "t2.medium"
  tags = {
    Env = "dev"
  }
}