/**
* Use this file to define the module input variables
* use the description field within each block to define
* the purpose, and the default field to define a default value
**/

variable "ami_id" {
  default = "ami-830c94e3"
  type = string
  description = "The AMI ID to create the ec2 from"
}

variable "instance_type" {
  default = "t2.micro"
  type = string
  description = "the type of the ec2 instance"
}

variable "tags" {
  type = map
  description = "the tags to attach to the module resources"
}