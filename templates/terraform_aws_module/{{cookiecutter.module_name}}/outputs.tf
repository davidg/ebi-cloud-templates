/**
* Use this file to export values from the resources, the values
* exported here will be logged in the terraform apply output
**/

output "ec2_instance_public_ip" {
  value = aws_instance.ec2_instance.public_ip
}