/**
* The main file contains the main resources in a module
* for bigger module you can create a file  per service. i.e:
* ec2.tf, iam.tf, eks.tf etc.
* This file contains the definition of a single ec2 instance
* all the values of the resources block are referencing
* module variables. In addition the random_pet block
* is defining a random id to use in the instance name
**/

resource "random_pet" "s3_bucket_id" {}

resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id
  instance_type = var.instance_type
  tags = var.tags
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "bkt-${random_pet.s3_bucket_id.id}"
  tags = var.tags
}