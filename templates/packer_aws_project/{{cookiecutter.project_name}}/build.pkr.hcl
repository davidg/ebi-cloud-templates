/**
* This file contains the build providers, We are importing the source and provisioning it with
* diferent sources: shell, files
**/

build {
  sources = [
    "source.amazon-ebs.seed",
  ]

  // copying the file to the remote machine
  provisioner "file" {
    source      = "files/.bashrc"
    destination = "~/.bashrc"
  }

  // run the script in the remote machine
  provisioner "shell" {
    scripts          = ["./scripts/install.sh"]
    environment_vars = ["FOO=bar"]
  }
}
