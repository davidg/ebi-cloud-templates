packer {
  required_plugins {
    amazon = {
      version = "{{ cookiecutter.aws_provider_version }}"
      source  = "github.com/hashicorp/amazon"
    }
  }
}
