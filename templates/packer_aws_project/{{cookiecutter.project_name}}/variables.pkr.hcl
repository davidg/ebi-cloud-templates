variable "region" {
  description = "The AWS region to build and publish the image"
  type        = string
  default     = "eu-west-2"
}

variable "version" {
  description = "The AMI version number"
  type        = string
}

variable "volume_size_gb" {
  description = "size of the disk attached to the build machine"
  default     = 20
}

variable "ssh_username" {
  default = "ubuntu"
  description = "The user used to SSH into the build machine"
}

variable spot_instance_types {
  description = "Instances types allowed to spot instances"
  default = [
    "t2.small"
  ]
}

variable instance_type {
  default     = "t2.nano"
  description = "The EC2 instance type to build the AMI"
  type        = string
}

variable source_ami {
  type = object({
    name  = string
    owner = string
  })
  default = {
    name  = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
    owner = "099720109477"
  }
}

variable ou_arns {
  type = list(string)
  description = "Organizational units with access to the AMI"
  default = []
}

variable role_arn {
  default = env("AWS_ROLE_ARN")
  description = ""
}

variable session_id {
  default = env("AWS_ROLE_SESSION_NAME")
}

variable commit_sha {
  default = "NO_COMMIT"
}