/**
* This file contains the target AMI details and the AMI source,
* the source_ami_filter defines the source AMI details, the rest of parameters
* are related to the target AMI
**/

// local variables and validations
locals {
  // random uid if you don't pass the version variable
  ami_uid          = split("-", uuidv4())[0]
  // if a version number isn't provided the version is created from uid and date
  ami_version           = coalesce( var.version, "${local.ami_uid}-${formatdate("YYYYMMDDHHMM", timestamp())}" )
  // the name of the ami, you can use a path-like name i.e: ebi/group-bio/conda-3.1.1
  ami_name_prefix       = coalesce( var.ami_name, "ebi/seed" )
  ami_name              = "${local.ami_name}-${local.ami_version}"
  ami_description       = "This ami: ${ami_name} was created from the commit: ${var.commit_sha}"
}

source "amazon-ebs" "seed" {
  // target AMI details
  ami_name        = local.ami_name
  instance_type   = var.instance_type
  ssh_username    = var.ssh_username
  ami_description = local.ami_description
  region          = var.region

  // filter the source AMI
  source_ami_filter {
    filters = {
      name                = var.source_ami.name
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = [ var.source_ami.owner ]
  }

  /* assuming the role in the target AWS Account
   * If you are testing this from the local machine 
   * comment this block or use a custom role and export the ARN
   * to AWS_ROLE_ARN env variable
    */
  assume_role {
    role_arn     = var.role_arn
    session_name = var.session_id
  }

  // Modifying the size of the disk attached to the machine
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = var.volume_size_gb
    delete_on_termination = true
  }
}
