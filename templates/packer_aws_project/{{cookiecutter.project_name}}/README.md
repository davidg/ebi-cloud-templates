# {{ cookiecutter.project_name }}

**{{ cookiecutter.project_name }}** Packer project.

# Getting Started

You can deploy the image from your local machine or create a Merge request to the **master** branch of the
(Images automation repository)[https://gitlab.ebi.ac.uk/cc-projects/images-automation] under the aws folder.
Before create the merge request please run the validate make task.

# Makefile

You can validate the packer project by running the command

```make validate```
